fig:nice-fig
tab:nice-tab
eq:binom
thm:tri
about
project-description
services-description
preview-book
hello-bookdown
a-section
cross
chapters-and-sub-chapters
captioned-figures-and-tables
parts
footnotes-and-citations
footnotes
citations
blocks
equations
theorems-and-proofs
callout-blocks
sharing-your-book
publishing
pages
metadata-for-sharing
deliverables
cover-image-path-to-the-social-sharing-image-like-imagescover.jpg
data-collection
